package com.amith.animalsounds.Fragments;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amith.animalsounds.Adapters.CustomListAdapter;
import com.amith.animalsounds.ModelClass.Animals;
import com.amith.animalsounds.R;

import java.util.ArrayList;


public class Birds extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<Animals> animalsList;

    int animalImage[] = {R.drawable.bee, R.drawable.canary, R.drawable.cardinal, R.drawable.crow, R.drawable.cricket, R.drawable.duck, R.drawable.eagle1, R.drawable.goose,
            R.drawable.housefly, R.drawable.macaw_parrot, R.drawable.mosquito, R.drawable.ninghtingale, R.drawable.owl, R.drawable.peacock,
            R.drawable.seagulls, R.drawable.vulture};

    String animalName[] = {"Bee", "Canary", "Cardinal", "Crow", "Cricket", "Duck", "Eagle", "Goose", "House Fly", "Parrot", "Mosquito", "Nightingale", "Owl", "Peacock",
            "Sea gulls", "Vulture"};

    String soundNames[] = {"hum", "...", "...", "caw", "chirp", "quack", "scream", "cackle", "buzz", "chatter", "...", "sing", "hoot", "...", "scream", "scream"};
    int sounds[] = {R.raw.bee, R.raw.canary, R.raw.cardinal, R.raw.crow, R.raw.cricket, R.raw.duck, R.raw.eagle, R.raw.geese, R.raw.housefly, R.raw.parrot, R.raw.mosquito,
            R.raw.nightingale, R.raw.owl, R.raw.peacock, R.raw.seagulls, R.raw.vulture};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.animal_layout, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        animalsList = new ArrayList<>();

        for(int index = 0; index < animalImage.length; index++){
            Animals animals = new Animals(animalImage[index], sounds[index], animalName[index], soundNames[index]);
            animalsList.add(animals);
        }

        CustomListAdapter adapter = new CustomListAdapter(getActivity(), animalsList);
        recyclerView.setAdapter(adapter);
    }
}
