package com.amith.animalsounds.Fragments;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amith.animalsounds.Adapters.CustomListAdapter;
import com.amith.animalsounds.ModelClass.Animals;
import com.amith.animalsounds.R;

import java.util.ArrayList;

public class Aquatic extends Fragment {

   private RecyclerView recyclerView;
   private ArrayList<Animals> animalsList;

    int animalImage[] = {R.drawable.crocodile, R.drawable.dolphin, R.drawable.penguins, R.drawable.seal, R.drawable.sea_lion};
    String animalName[] = {"Crocodile", "Dolphin", "Penguins", "Seal", "Sea Lion"};
    String soundNames[] = {"...", "click", "gaw", "bark", "brawling"};
    int sounds[] = {R.raw.crocodile, R.raw.dolphin, R.raw.penguin, R.raw.seal, R.raw.sealion};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.animal_layout, container, false);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        animalsList = new ArrayList<>();

        for(int index = 0; index < animalImage.length; index++){
            Animals animals = new Animals(animalImage[index], sounds[index], animalName[index], soundNames[index]);
            animalsList.add(animals);
        }

        CustomListAdapter adapter = new CustomListAdapter(getActivity(), animalsList);
        recyclerView.setAdapter(adapter);
    }
}