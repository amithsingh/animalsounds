package com.amith.animalsounds.Fragments;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amith.animalsounds.Adapters.CustomListAdapter;
import com.amith.animalsounds.ModelClass.Animals;
import com.amith.animalsounds.R;

import java.util.ArrayList;


public class WildAnimals extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<Animals> animalsList;

    int animalImage[] = {R.drawable.bear, R.drawable.camel, R.drawable.cougar, R.drawable.chimpanzee, R.drawable.elephant, R.drawable.gibbon, R.drawable.gorilla, R.drawable.hippopotamus,
            R.drawable.jaguar, R.drawable.leapord, R.drawable.lion, R.drawable.panther, R.drawable.racoon, R.drawable.rihnocerous, R.drawable.snake, R.drawable.tiger1, R.drawable.wolf, R.drawable.zebra};

    String animalName[] = {"Bear", "Camel", "Cougar", "Chimpanzee", "Elephant", "Gibbon", "Gorilla", "Hippopotamus", "Jaguar", "Leopard", "Lion", "Panther","Raccoon", "Rhinoceros", "Snake", "Tiger", "Wolf", "Zebra"};

    String soundNames[] = {"growl", "grunt", "growl", "scream", "trumpet", "...", "...", "growl", "growl", "growl", "roar", "growl","trill", "bellow", "hiss", "growl", "howl", "whinny"};
    int sounds[] = {R.raw.bear, R.raw.camel, R.raw.cougar, R.raw.chimpanzee, R.raw.elephant, R.raw.gibbon, R.raw.gorilla, R.raw.hippopotamus, R.raw.jaguar,
            R.raw.leopard, R.raw.lion, R.raw.panther,R.raw.raccoon, R.raw.rhinoceros, R.raw.snake, R.raw.tiger, R.raw.wolf, R.raw.zebra};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.animal_layout, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        animalsList = new ArrayList<>();

        for(int index = 0; index < animalImage.length; index++){
            Animals animals = new Animals(animalImage[index], sounds[index], animalName[index], soundNames[index]);
            animalsList.add(animals);
        }

        CustomListAdapter adapter = new CustomListAdapter(getActivity(), animalsList);
        recyclerView.setAdapter(adapter);
    }
}

