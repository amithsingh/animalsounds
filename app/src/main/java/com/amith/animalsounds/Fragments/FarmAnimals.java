package com.amith.animalsounds.Fragments;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amith.animalsounds.Adapters.CustomListAdapter;
import com.amith.animalsounds.ModelClass.Animals;
import com.amith.animalsounds.R;

import java.util.ArrayList;


public class FarmAnimals extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<Animals> animalsList;

    int animalImage[] = {R.drawable.cat, R.drawable.chick, R.drawable.cow, R.drawable.dog, R.drawable.donkey, R.drawable.frog,
            R.drawable.goat, R.drawable.hen, R.drawable.horse, R.drawable.monkey, R.drawable.pig, R.drawable.sheep, R.drawable.squirrel};

    String animalName[] = {"Cat", "Chick", "Cow", "Dog", "Donkey", "Frog", "Goat", "Hen", "Horse", "Monkey", "Pig", "Sheep", "Squirrel"};

    int sounds[] = {R.raw.cat, R.raw.chick, R.raw.cow, R.raw.dog, R.raw.donkey, R.raw.frog, R.raw.goat,
            R.raw.hen, R.raw.horse, R.raw.monkey, R.raw.pig, R.raw.sheep, R.raw.squirrel};

    String soundNames[] = {"meow", "cheep", "moo", "bark", "bray", "croak", "bleat", "cackle", "neigh", "chatter", "snort", "bleat", "squeak"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.animal_layout, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        animalsList = new ArrayList<>();

        for(int index = 0; index < animalImage.length; index++){
            Animals animals = new Animals(animalImage[index], sounds[index], animalName[index], soundNames[index]);
            animalsList.add(animals);
        }

        CustomListAdapter adapter = new CustomListAdapter(getActivity(), animalsList);
        recyclerView.setAdapter(adapter);
    }

//    private void selectResources(int pos){
////        if(pos == 1){
////            // Farm Animals
////            int animalImage[] = {R.drawable.cat, R.drawable.chick, R.drawable.cow, R.drawable.dog, R.drawable.donkey, R.drawable.frog,
////                    R.drawable.goat, R.drawable.hen, R.drawable.horse, R.drawable.monkey, R.drawable.pig, R.drawable.sheep, R.drawable.squirrel};
////
////            String animalName[] = {"Cat", "Chick", "Cow", "Dog", "Donkey", "Frog", "Goat", "Hen", "Horse", "Monkey", "Pig", "Sheep", "Squirrel"};
////
////            int sounds[] = {R.raw.cat, R.raw.chick, R.raw.cow, R.raw.dog, R.raw.donkey, R.raw.frog, R.raw.goat,
////                    R.raw.hen, R.raw.horse, R.raw.monkey, R.raw.pig, R.raw.sheep, R.raw.squirrel};
////
////            String soundNames[] = {"meow", "cheep", "moo", "bark", "bray", "croak", "bleat", "cackle", "neigh", "chatter", "snort", "bleat", "squeak"};
////
////        } else if(pos ==2){
////            // Wild animals
////            int animalImage[] = {R.drawable.bear, R.drawable.camel, R.drawable.cougar, R.drawable.chimpanzee, R.drawable.elephant, R.drawable.gibbon, R.drawable.gorilla, R.drawable.hippopotamus,
////                    R.drawable.jaguar, R.drawable.leapord, R.drawable.lion, R.drawable.panther, R.drawable.racoon, R.drawable.rihnocerous, R.drawable.snake, R.drawable.tiger1, R.drawable.wolf, R.drawable.zebra};
////
////            String animalName[] = {"Bear", "Camel", "Cougar", "Chimpanzee", "Elephant", "Gibbon", "Gorilla", "Hippopotamus", "Jaguar", "Leopard", "Lion", "Panther","Raccoon", "Rhinoceros", "Snake", "Tiger", "Wolf", "Zebra"};
////
////            String soundNames[] = {"growl", "grunt", "growl", "scream", "trumpet", "...", "...", "growl", "growl", "growl", "roar", "growl","trill", "bellow", "hiss", "growl", "howl", "whinny"};
////            int sounds[] = {R.raw.bear, R.raw.camel, R.raw.cougar, R.raw.chimpanzee, R.raw.elephant, R.raw.gibbon, R.raw.gorilla, R.raw.hippopotamus, R.raw.jaguar,
////                    R.raw.leopard, R.raw.lion, R.raw.panther,R.raw.raccoon, R.raw.rhinoceros, R.raw.snake, R.raw.tiger, R.raw.wolf, R.raw.zebra};
////
////        } else if(pos == 3){
////            // Birds
////            int animalImage[] = {R.drawable.bee, R.drawable.canary, R.drawable.cardinal, R.drawable.crow, R.drawable.cricket, R.drawable.duck, R.drawable.eagle1, R.drawable.goose,
////                    R.drawable.housefly, R.drawable.macaw_parrot, R.drawable.mosquito, R.drawable.ninghtingale, R.drawable.owl, R.drawable.peacock,
////                    R.drawable.seagulls, R.drawable.vulture};
////
////            String animalName[] = {"Bee", "Canary", "Cardinal", "Crow", "Cricket", "Duck", "Eagle", "Goose", "House Fly", "Parrot", "Mosquito", "Nightingale", "Owl", "Peacock",
////                    "Sea gulls", "Vulture"};
////
////            String soundNames[] = {"hum", "...", "...", "caw", "chirp", "quack", "scream", "cackle", "buzz", "chatter", "...", "sing", "hoot", "...", "scream", "scream"};
////            int sounds[] = {R.raw.bee, R.raw.canary, R.raw.cardinal, R.raw.crow, R.raw.cricket, R.raw.duck, R.raw.eagle, R.raw.geese, R.raw.housefly, R.raw.parrot, R.raw.mosquito,
////                    R.raw.nightingale, R.raw.owl, R.raw.peacock, R.raw.seagulls, R.raw.vulture};
////
////        } else if(pos == 4){
////            // Aquatic Animals
////            int animalImage[] = {R.drawable.crocodile, R.drawable.dolphin, R.drawable.penguins, R.drawable.seal, R.drawable.sea_lion};
////            String animalName[] = {"Crocodile", "Dolphin", "Penguins", "Seal", "Sea Lion"};
////            String soundNames[] = {"...", "click", "gaw", "bark", "brawling"};
////            int sounds[] = {R.raw.crocodile, R.raw.dolphin, R.raw.penguin, R.raw.seal, R.raw.sealion};
////        }
////
////        for(int index = 0; index < animalImage.length; index++){
////            Animals animals = new Animals(animalImage[index], sounds[index], animalName[index], soundNames[index]);
////            animalsList.add(animals);
////        }
////
////    }
}