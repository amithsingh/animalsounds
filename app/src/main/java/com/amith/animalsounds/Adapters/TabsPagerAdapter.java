package com.amith.animalsounds.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.amith.animalsounds.Fragments.Aquatic;
import com.amith.animalsounds.Fragments.Birds;
import com.amith.animalsounds.Fragments.FarmAnimals;
import com.amith.animalsounds.Fragments.WildAnimals;

/**
 * Created by Aravind on 02-11-2017.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm){
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new FarmAnimals();
            case 1: return new WildAnimals();
            case 2: return new Birds();
            case 3: return new Aquatic();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
