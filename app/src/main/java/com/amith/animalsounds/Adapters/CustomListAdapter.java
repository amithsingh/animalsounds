package com.amith.animalsounds.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.amith.animalsounds.ModelClass.Animals;
import com.amith.animalsounds.R;

import java.util.ArrayList;

/**
 * Created by Aravind on 29-10-2017.
 */

public class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Animals> animalsList;
    private LayoutInflater inflater;
    public static MediaPlayer mp3Player;


    public CustomListAdapter(Context context, ArrayList<Animals> animalsList) {
        this.context = context;
        this.animalsList = animalsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Animals animals = animalsList.get(position);
        holder.name.setText(animals.getAnimalName());
        holder.image.setImageResource(animals.getAnimalImage());
        holder.soundName.setText(animals.getSoundName());

        String fontPath = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
        holder.name.setTypeface(tf);

        holder.sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // playClip(holder.sound, animals);
                if (mp3Player != null) {
                    mp3Player.stop();
                    mp3Player.release();
                    mp3Player = null;
                }
                mp3Player = MediaPlayer.create(context, animals.getAnimalSound());
                mp3Player.start();
            }
        });
        holder.stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp3Player != null) {
                    mp3Player.stop();
                    mp3Player.release();
                    mp3Player = null;
                }
            }
        });
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog builder = new Dialog(context);
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                builder.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                builder.setContentView(R.layout.custom_dialog);

                ImageView iv = builder.findViewById(R.id.image_dialog);
                TextView tv = builder.findViewById(R.id.text_dialog);
                ImageView sound = builder.findViewById(R.id.play_dialog);

                iv.setImageResource(animals.getAnimalImage());
                tv.setText(animals.getAnimalName());
                playClip(sound, animals);

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        if (mp3Player != null) {
                            mp3Player.stop();
                        }
                    }
                });

                builder.show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return animalsList.size();
    }

    public void playClip(ImageView button, final Animals animals) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp3Player != null) {
                    mp3Player.stop();
                    mp3Player.release();
                    mp3Player = null;
                }
                mp3Player = MediaPlayer.create(context, animals.getAnimalSound());
                mp3Player.start();
            }
        });
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public ImageView sound;
        public ImageView stop;
        public TextView name;
        public TextView soundName;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageView);
            sound = itemView.findViewById(R.id.sound);
            name = itemView.findViewById(R.id.textView1);
            soundName = itemView.findViewById(R.id.textView2);
            stop = itemView.findViewById(R.id.stop);

        }
    }
}