package com.amith.animalsounds.ModelClass;


/**
 * Created by Aravind on 14-11-2017.
 */

public class Animals {

    private int animalImage, animalSound;
    private String animalName, soundName;

    public Animals(int animalImage, int animalSound, String animalName, String soundName) {
        this.animalImage = animalImage;
        this.animalSound = animalSound;
        this.animalName = animalName;
        this.soundName = soundName;
    }

    public int getAnimalImage() {
        return animalImage;
    }

    public int getAnimalSound() {
        return animalSound;
    }

    public String getAnimalName() {
        return animalName;
    }

    public String getSoundName() {
        return soundName;
    }

    public void setAnimalImage(int animalImage) {
        this.animalImage = animalImage;
    }

    public void setAnimalSound(int animalSound) {
        this.animalSound = animalSound;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }
}
